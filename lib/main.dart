import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:space_lab/navigation/parser/parser_route_strategy_helper.dart';
import 'package:space_lab/repository/launches_cache.dart';
import 'package:space_lab/repository/launches_parser.dart';
import 'package:space_lab/repository/launches_repository.dart';
import 'package:space_lab/service/launches_service.dart';
import 'package:space_lab/utils/bloc_observer.dart';
import 'package:space_lab/utils/keys.dart';
import 'package:space_lab/utils/localization/localization.dart';
import 'package:space_lab/utils/style/theme.dart';
import 'package:space_lab/utils/utils.dart';

import 'navigation/back_button_dispatcher.dart';
import 'navigation/parser/space_lab_route_info_parser.dart';
import 'navigation/space_lab_router_delegate.dart';
import 'navigation/strem/image_url_strem_controller.dart';
import 'navigation/strem/launch_id_strem_controller.dart';

Future<void> main() async {
  Bloc.observer = SimpleBlocObserver();
  WidgetsFlutterBinding.ensureInitialized();
  ResponsiveSizingConfig.instance.setCustomBreakpoints(
    const ScreenBreakpoints(desktop: 800, tablet: 800, watch: 200),
  );
  runApp(
    MultiProvider(
      providers: [
        Provider<GraphQLClient>(
          create: (context) => GraphQLClient(
            cache: GraphQLCache(),
            link: HttpLink(
              launchesUrl,
            ),
          ),
        ),
        Provider<LaunchesParser>(
          create: (context) => LaunchesParserImpl(),
        ),
        Provider<LaunchesService>(
          create: (context) => LaunchesServiceImpl(
            client: context.read<GraphQLClient>(),
          ),
        ),
        Provider<LaunchesCache>(
          create: (context) => LaunchesCacheImpl(),
        ),
        Provider<LaunchesRepository>(
          create: (context) => LaunchesRepositoryImpl(
            service: context.read<LaunchesService>(),
            parser: context.read<LaunchesParser>(),
            cache: context.read<LaunchesCache>(),
          ),
        ),
        Provider<LaunchIdProvider>(
          create: (context) => LaunchIdProviderImpl(),
        ),
        Provider<ImageUrlProvider>(
          create: (context) => ImageUrlProviderImpl(),
        ),
        Provider<ParserRouteStrategyHelper>(
          create: (context) => ParserRouteStrategyHelperImpl(),
        ),
        Provider<SpaceLabRouteInformationParser>(
          create: (context) => SpaceLabRouteInformationParser(
            parserRouteStrategyHelper: context.read<ParserRouteStrategyHelper>(),
          ),
        ),
        ListenableProvider<SpaceLabRouterDelegate>(
          create: (context) => SpaceLabRouterDelegate(
            launchIdProvider: context.read<LaunchIdProvider>(),
            imageUrlProvider: context.read<ImageUrlProvider>(),
          ),
        ),
        Provider<SpaceLabBackButtonDispatcher>(
          create: (context) => SpaceLabBackButtonDispatcher(
            routerDelegate: context.read<SpaceLabRouterDelegate>(),
          ),
        ),
      ],
      child: const SpaceLabApp(
        key: SpaceLabKeys.appKey,
      ),
    ),
  );
}

class SpaceLabApp extends StatelessWidget {
  const SpaceLabApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: (SpaceLabLocalizations.of(context)?.appTitle).orEmpty,
      theme: SpaceLabTheme.theme,
      supportedLocales: const [Locale("en", "US"), Locale("en", "EN"), Locale("ru", "RU")],
      localizationsDelegates: [
        SpaceLabLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      routerDelegate: context.read<SpaceLabRouterDelegate>(),
      routeInformationParser: context.read<SpaceLabRouteInformationParser>(),
      backButtonDispatcher: context.read<SpaceLabBackButtonDispatcher>(),
    );
  }
}
