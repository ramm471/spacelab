import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListBottomLoadingWidget extends StatelessWidget {
  const ListBottomLoadingWidget({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("ListBottomLoadingWidget build");
    return Container(
      height: 50.0,
      color: Colors.transparent,
      child: Center(
        child: CircularProgressIndicator(
          color: Colors.deepPurple[600] ?? Colors.deepPurple,
          backgroundColor: Colors.transparent,
        ),
      ),
    );
  }
}
