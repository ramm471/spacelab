import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListEmptyLoadingWidget extends StatelessWidget {
  const ListEmptyLoadingWidget({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("ListEmptyLoadingWidget build");
    return Expanded(
      child: Center(
        child: CircularProgressIndicator(
          color: Colors.deepPurple[600] ?? Colors.deepPurple,
          backgroundColor: Colors.transparent,
        ),
      ),
    );
  }
}
