import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:space_lab/list/bloc/list_cubit.dart';
import 'package:space_lab/list/model/launch_model.dart';
import 'package:space_lab/navigation/strem/launch_id_strem_controller.dart';
import 'package:space_lab/utils/keys.dart';
import 'package:visibility_detector/visibility_detector.dart';

import '../list_item/launches_item.dart';

class LaunchesListWidget extends StatelessWidget {
  final List<SpaceLaunch> _items;

  const LaunchesListWidget({
    required Key key,
    required List<SpaceLaunch> items,
  })  : _items = items,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("LaunchesListWidget build");
    return Expanded(
      child: ListView.builder(
        key: const PageStorageKey<String>('LaunchesListWidget'),
        controller: ScrollController(),
        itemCount: _items.length + 1,
        addAutomaticKeepAlives: true,
        itemBuilder: (context, index) {
          if (index < _items.length) {
            final item = _items[index];
            return LaunchesItem(
              key: SpaceLabKeys.launchItem(item.id),
              item: item,
              onTap: () {
                debugPrint("onTap $item");
                context.read<LaunchIdProvider>().selectValue(item.id);
              },
            );
          } else {
            return VisibilityDetector(
              key: SpaceLabKeys.launchesScreenVisibilityKey,
              onVisibilityChanged: (visibilityInfo) {
                final visiblePercentage = visibilityInfo.visibleFraction * 100;
                debugPrint('ListScreen VisibilityDetector ${visibilityInfo.key} is $visiblePercentage% visible');
                if (visiblePercentage == 100) {
                  BlocProvider.of<ListCubit>(context).loadNextPage();
                }
              },
              child: const SizedBox(
                height: 1,
              ),
            );
          }
        },
      ),
    );
  }
}
