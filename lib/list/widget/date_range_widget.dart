import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:space_lab/utils/style/text_style.dart';

class DateRangeWidget extends StatefulWidget {
  const DateRangeWidget({Key? key}) : super(key: key);

  @override
  State<DateRangeWidget> createState() => _DateRangeWidgetState();
}

class _DateRangeWidgetState extends State<DateRangeWidget> {
  DateTime _from = DateTime.parse("2006-01-01");
  DateTime _to = DateTime.now();

  @override
  Widget build(BuildContext context) {
    log("build from: $_from to: $_to");
    return SizedBox(
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _buildPickerButton(
            context: context,
            selected: _from,
            min: DateTime.parse("2006-01-01"),
            max: _to,
            onSelected: (dateTime) {
              setState(() {
                _from = dateTime;
              });
            },
          ),
          _buildPickerButton(
            context: context,
            selected: _to,
            min: _from,
            max: DateTime.now(),
            onSelected: (dateTime) {
              setState(() {
                _to = dateTime;
              });
            },
          ),
        ],
      ),
    );
  }

  TextButton _buildPickerButton({
    required BuildContext context,
    required DateTime selected,
    required DateTime min,
    required DateTime max,
    required Function(DateTime) onSelected,
  }) {
    return TextButton(
      style: TextButton.styleFrom(
        primary: Colors.deepPurple[400] ?? Colors.deepPurple,
        backgroundColor: Colors.deepPurple[600] ?? Colors.deepPurple,
        onSurface: Colors.grey,
      ),
      onPressed: () {
        DatePicker.showDatePicker(
          context,
          showTitleActions: true,
          minTime: min,
          maxTime: max,
          onConfirm: (date) {
            log('confirm $date');
            onSelected(date);
          },
          currentTime: selected,
          locale: LocaleType.en,
        );
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
        child: Text(
          DateFormat('yyyy-MM-dd').format(selected),
          softWrap: true,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: SpaceLabTextStyle.headline4PrimaryTextStyle(context),
        ),
      ),
    );
  }
}
