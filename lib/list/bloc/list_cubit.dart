import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:space_lab/list/model/launch_model.dart';
import 'package:space_lab/repository/entity/launch_entity.dart';
import 'package:space_lab/repository/launches_repository.dart';

import 'list_state.dart';

class ListCubit extends Cubit<ListState> {
  final LaunchesRepository _launchesRepository;
  final List<SpaceLaunch> _items = [];

  ListCubit({required LaunchesRepository launchesRepository})
      : _launchesRepository = launchesRepository,
        super(const ListScreenInitialState()) {
    loadNextPage();
  }

  Future<void> loadNextPage() async {
    debugPrint("loadList");
    if (state is ListScreenLoadingState) return;
    emit(ListScreenLoadingState(launches: List.of(_items)));

    final newList = await _launchesRepository.loadLaunches(offset: _items.length).onError((error, stackTrace) {
      emit(ListScreenErrorState(launches: List.of(_items)));
      return [];
    });
    debugPrint("_loadLaunches list: $newList");
    if (newList.isNotEmpty) {
      _items.addAll(_mapToUIItems(newList));
    }
    emit(ListScreenPageLoadedState(launches: List.of(_items)));
  }

  List<SpaceLaunch> _mapToUIItems(List<LaunchEntity> entities) {
    return entities.map((e) => SpaceLaunch.fromEntity(e)).toList();
  }
}
