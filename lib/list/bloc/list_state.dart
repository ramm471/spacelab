import 'package:equatable/equatable.dart';
import 'package:space_lab/list/model/launch_model.dart';

abstract class ListState extends Equatable {
  const ListState();

  @override
  List<Object> get props => [];
}

abstract class ListLoadedState extends ListState {
  final List<SpaceLaunch> launches;

  const ListLoadedState({
    required this.launches,
  }) : super();

  @override
  List<Object> get props => [launches];

  @override
  String toString() {
    return 'ListLoadedState{launches: size: $launches';
  }
}

class ListScreenInitialState extends ListState {
  const ListScreenInitialState() : super();
}

class ListScreenErrorState extends ListLoadedState {
  const ListScreenErrorState({required List<SpaceLaunch> launches}) : super(launches: launches);

  @override
  String toString() {
    return 'ListScreenErrorState{launches: size: ${launches.length}';
  }
}

class ListScreenPageLoadedState extends ListLoadedState {
  const ListScreenPageLoadedState({required List<SpaceLaunch> launches}) : super(launches: launches);

  @override
  String toString() {
    return 'ListScreenPageLoadedState{launches: size: ${launches.length}';
  }
}

class ListScreenLoadingState extends ListLoadedState {
  const ListScreenLoadingState({required List<SpaceLaunch> launches}) : super(launches: launches);

  @override
  String toString() {
    return 'ListScreenLoadingState{launches: size: ${launches.length}';
  }
}
