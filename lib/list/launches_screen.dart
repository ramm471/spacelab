import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:space_lab/list/widget/date_range_widget.dart';
import 'package:space_lab/list/widget/launches_list_widget.dart';
import 'package:space_lab/list/widget/list_bottom_loading_widget.dart';
import 'package:space_lab/list/widget/list_empty_loading_widget.dart';
import 'package:space_lab/utils/keys.dart';
import 'package:space_lab/utils/localization/localization.dart';
import 'package:space_lab/utils/utils.dart';

import 'bloc/list_cubit.dart';
import 'bloc/list_state.dart';

class ListScreen extends StatelessWidget {
  const ListScreen({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("ListScreen build");
    return BlocConsumer<ListCubit, ListState>(
      listener: (context, state) {
        debugPrint("ListScreen Consumer state: $state");
        if (state is ListScreenErrorState) {
          _showError(context);
        }
      },
      builder: (context, state) {
        debugPrint("ListScreen builder state: $state");
        return Column(
          children: <Widget>[
            const DateRangeWidget(),
            if (state is ListScreenInitialState || (state is ListScreenLoadingState && state.launches.isEmpty))
              const ListEmptyLoadingWidget(key: SpaceLabKeys.launchesScreenEmptyLoadingKey),
            if (state is ListLoadedState && state.launches.isNotEmpty)
              LaunchesListWidget(
                key: SpaceLabKeys.launchesScreenListKey,
                items: state.launches,
              ),
            if (state is ListScreenLoadingState && state.launches.isNotEmpty)
              const ListBottomLoadingWidget(
                key: SpaceLabKeys.launchesScreenBottomLoadingKey,
              ),
          ],
        );
      },
    );
  }

  void _showError(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          (SpaceLabLocalizations.of(context)?.error).orEmpty,
        ),
      ),
    );
  }
}
