import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:space_lab/repository/entity/rocket_entity.dart';

part 'rocket_model.freezed.dart';

@Freezed()
class Rocket with _$Rocket {
  const factory Rocket({
    String? id,
    String? name,
    bool? isActive,
    int? costPerLaunch,
    String? country,
    String? description,
    String? company,
    int? mass,
    double? diameter,
  }) = _Rocket;

  static Rocket fromEntity(RocketEntity entity) {
    return Rocket(
      id: entity.id,
      name: entity.name,
      isActive: entity.isActive,
      costPerLaunch: entity.costPerLaunch,
      country: entity.country,
      description: entity.description,
      company: entity.company,
      mass: entity.mass?.massKg,
      diameter: entity.diameter?.meters,
    );
  }
}
