import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:intl/intl.dart';
import 'package:space_lab/list/model/rocket_model.dart';
import 'package:space_lab/repository/entity/launch_entity.dart';

part 'launch_model.freezed.dart';

@Freezed()
class SpaceLaunch with _$SpaceLaunch {
  const factory SpaceLaunch({
    required String id,
    required String missionName,
    required String? missionDescription,
    required DateTime launchDateUtc,
    required String formattedDate,
    required String formattedTime,
    List<String>? links,
    List<String>? images,
    Rocket? rocket,
  }) = _SpaceLaunch;

  static SpaceLaunch fromEntity(LaunchEntity entity) {
    var rocketEntityOptional = entity.rocket?.rocket;
    return SpaceLaunch(
      id: entity.id,
      missionName: entity.missionName,
      missionDescription: entity.description,
      launchDateUtc: entity.launchDateUtc,
      formattedDate: DateFormat('yyyy-MM-dd').format(entity.launchDateUtc),
      formattedTime: DateFormat('kk:mm').format(entity.launchDateUtc),
      links: <String?>[entity.links?.articleLink, entity.links?.wikiLink].whereType<String>().toList(),
      images: List.of(entity.links?.images ?? []).whereType<String>().toList(),
      rocket: rocketEntityOptional != null ? Rocket.fromEntity(rocketEntityOptional) : null,
    );
  }
}
