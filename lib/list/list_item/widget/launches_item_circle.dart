import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LaunchesItemCircle extends StatelessWidget {
  const LaunchesItemCircle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 8,
      height: 8,
      decoration: BoxDecoration(
        color: Colors.grey[600] ?? Colors.black,
        shape: BoxShape.circle,
      ),
      child: Container(
        margin: const EdgeInsets.all(2),
        width: 2,
        height: 2,
        decoration: const BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
      ),
    );
  }
}
