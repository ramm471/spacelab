import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LaunchesItemCircleSwatch extends StatelessWidget {
  final int _swatch;

  const LaunchesItemCircleSwatch({
    Key? key,
    required int swatch,
  })  : _swatch = swatch,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16,
        top: 4,
      ),
      child: Container(
        width: 2,
        height: 2,
        decoration: BoxDecoration(
          color: Colors.grey[_swatch] ?? Colors.grey,
          shape: BoxShape.circle,
        ),
      ),
    );
  }
}
