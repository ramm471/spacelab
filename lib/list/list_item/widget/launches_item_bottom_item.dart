import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:space_lab/utils/style/text_style.dart';

class LaunchesItemBottomItem extends StatelessWidget {
  final String? _hint;
  final String? _value;

  const LaunchesItemBottomItem({
    Key? key,
    String? hint,
    String? value,
  })  : _hint = hint,
        _value = value,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          (_hint ?? "").toUpperCase(),
          softWrap: true,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: SpaceLabTextStyle.body2SecondaryTextStyle(context),
        ),
        Text(
          _value ?? "",
          softWrap: true,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: SpaceLabTextStyle.subtitle2SecondaryTextStyle(context),
        ),
      ],
    );
  }
}
