import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:space_lab/list/list_item/widget/launches_item_bottom_item.dart';
import 'package:space_lab/list/list_item/widget/launches_item_circle.dart';
import 'package:space_lab/list/list_item/widget/launches_item_circle_small.dart';
import 'package:space_lab/utils/keys.dart';
import 'package:space_lab/utils/localization/localization.dart';
import 'package:space_lab/utils/style/text_style.dart';
import 'package:space_lab/utils/utils.dart';

import '../model/launch_model.dart';

class LaunchesItem extends StatelessWidget {
  final GestureTapCallback _onTap;
  final SpaceLaunch _item;

  const LaunchesItem({
    required Key key,
    required GestureTapCallback onTap,
    required SpaceLaunch item,
  })  : _onTap = onTap,
        _item = item,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final localization = SpaceLabLocalizations.of(context);
    return InkWell(
      key: SpaceLabKeys.launchItemInkWell(_item.id),
      child: Card(
        key: SpaceLabKeys.launchItemCard(_item.id),
        margin: const EdgeInsets.all(7.0),
        color: Colors.grey,
        elevation: 5,
        child: ClipPath(
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: Row(
              children: [
                Container(
                  height: itemExtent,
                  width: 90,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.deepPurple[400] ?? Colors.deepPurple,
                        Colors.deepPurple[600] ?? Colors.deepPurple,
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        _item.id,
                        textAlign: TextAlign.center,
                        style: SpaceLabTextStyle.headline4PrimaryTextStyle(context),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            _item.formattedTime,
                            textAlign: TextAlign.end,
                            style: SpaceLabTextStyle.body1PrimaryTextStyle(context),
                          ),
                          Text(
                            _item.formattedDate,
                            textAlign: TextAlign.end,
                            style: SpaceLabTextStyle.subtitle2PrimaryTextStyle(context),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          Column(
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(
                                  left: 16,
                                  top: 8,
                                ),
                                child: LaunchesItemCircle(),
                              ),
                              LaunchesItemCircleSwatch(swatch: 400),
                              LaunchesItemCircleSwatch(swatch: 500),
                              LaunchesItemCircleSwatch(swatch: 600),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: 16,
                                  top: 5,
                                ),
                                child: LaunchesItemCircle(),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 40, right: 8, top: 4),
                                child: Text(
                                  _item.missionName,
                                  softWrap: true,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: SpaceLabTextStyle.subtitle2PrimaryAlternativeTextStyle(context),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(left: 26, right: 8),
                                child: Divider(
                                  color: Colors.grey,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 40, right: 8, bottom: 4),
                                child: Text(
                                  (_item.rocket?.name).orEmpty,
                                  softWrap: true,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: SpaceLabTextStyle.body1PrimaryAlternativeTextStyle(context),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          LaunchesItemBottomItem(
                            hint: localization?.rocketDiameter,
                            value: (_item.rocket?.diameter ?? 0).toString(),
                          ),
                          LaunchesItemBottomItem(
                            hint: localization?.rocketActive,
                            value: (_item.rocket?.isActive ?? false).toString(),
                          ),
                          LaunchesItemBottomItem(
                            hint: localization?.costPL,
                            value: (_item.rocket?.costPerLaunch ?? 0).toString(),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          clipper: ShapeBorderClipper(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6),
            ),
          ),
        ),
      ),
      onTap: _onTap,
    );
  }
}

const double itemExtent = 120;
