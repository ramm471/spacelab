import 'dart:async';
import 'dart:core';

import 'package:flutter/foundation.dart' show debugPrint, kIsWeb;
import 'package:graphql/client.dart';
import 'package:space_lab/service/launches_queries.dart';

abstract class LaunchesService {
  Future<QueryResult> loadLaunches({
    required int offset,
  });

  Future<QueryResult> loadLaunchDetails(String id);
}

class LaunchesServiceImpl implements LaunchesService {
  final GraphQLClient _client;

  LaunchesServiceImpl({
    required GraphQLClient client,
  }) : _client = client;

  @override
  Future<QueryResult> loadLaunches({
    required int offset,
  }) async {
    debugPrint("loadLaunches offset: $offset;");

    final QueryResult result = await _client.query(
      QueryOptions(
        document: gql(readLaunches),
        variables: <String, dynamic>{
          'limit': launchesLimit,
          'offset': offset,
        },
      ),
    );
    return result;
  }

  @override
  Future<QueryResult> loadLaunchDetails(String id) async {
    final QueryResult result = await _client.query(
      QueryOptions(
        document: gql(readLaunchById),
        variables: <String, dynamic>{
          'id': id,
        },
      ),
    );

    return result;
  }
}

const String launchesUrl = "https://api.spacex.land/graphql";
const int launchesLimit = kIsWeb ? 15 : 10;
