const String readLaunches = r'''
  query readLaunches($limit: Int!, $offset: Int!) {
      launchesPast(limit: $limit, offset: $offset) {
        id
        mission_name
        launch_date_utc
        rocket {
          rocket {
            id
            name
            active
            cost_per_launch
            diameter {
              meters
            }
          }
        }
      }
  }
''';

const String readLaunchById = r'''
  query readLaunch($id: ID!) {
      launch(id: $id) {
        id
        mission_name
        details
        launch_date_utc
        links {
          article_link
          wikipedia
          flickr_images
        }
        rocket {
          rocket {
            id
            name
            active
            cost_per_launch
            diameter {
              meters
            }
            country
            description
            company
            mass {
              kg
            }
          }
        }
      }
  }
''';
