import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:space_lab/repository/entity/mass_entity.dart';
import 'package:space_lab/repository/entity/diameter_entity.dart';

part 'rocket_entity.freezed.dart';

part 'rocket_entity.g.dart';

@Freezed()
class RocketEntity with _$RocketEntity {
  @JsonSerializable(explicitToJson: true)
  const factory RocketEntity({
    @JsonKey(name: 'id') String? id,
    @JsonKey(name: 'name') String? name,
    @JsonKey(name: 'active') bool? isActive,
    @JsonKey(name: 'cost_per_launch') int? costPerLaunch,
    @JsonKey(name: 'country') String? country,
    @JsonKey(name: 'description') String? description,
    @JsonKey(name: 'company') String? company,
    @JsonKey(name: 'mass') MassEntity? mass,
    @JsonKey(name: 'diameter') DiameterEntity? diameter,
  }) = _RocketEntity;

  factory RocketEntity.fromJson(Map<String, dynamic> json) => _$RocketEntityFromJson(json);
}
