import 'package:freezed_annotation/freezed_annotation.dart';

part 'diameter_entity.freezed.dart';

part 'diameter_entity.g.dart';

@Freezed()
class DiameterEntity with _$DiameterEntity {
  @JsonSerializable(explicitToJson: true)
  const factory DiameterEntity({
    @JsonKey(name: 'meters') double? meters,
  }) = _DiameterEntity;

  factory DiameterEntity.fromJson(Map<String, dynamic> json) => _$DiameterEntityFromJson(json);
}
