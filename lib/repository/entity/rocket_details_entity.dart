import 'package:freezed_annotation/freezed_annotation.dart';

import 'rocket_entity.dart';

part 'rocket_details_entity.freezed.dart';

part 'rocket_details_entity.g.dart';

@Freezed()
class RocketDetailsEntity with _$RocketDetailsEntity {
  @JsonSerializable(explicitToJson: true)
  const factory RocketDetailsEntity({
    @JsonKey(name: 'rocket') RocketEntity? rocket,
  }) = _RocketDetailsEntity;

  factory RocketDetailsEntity.fromJson(Map<String, dynamic> json) => _$RocketDetailsEntityFromJson(json);
}
