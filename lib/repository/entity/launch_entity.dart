import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:space_lab/repository/entity/rocket_details_entity.dart';

import 'links_entity.dart';

part 'launch_entity.freezed.dart';

part 'launch_entity.g.dart';

@Freezed()
class LaunchEntity with _$LaunchEntity {
  @JsonSerializable(explicitToJson: true)
  const factory LaunchEntity({
    @JsonKey(name: 'id') required String id,
    @JsonKey(name: 'mission_name') required String missionName,
    @JsonKey(name: 'launch_date_utc') required DateTime launchDateUtc,
    @JsonKey(name: 'details') String? description,
    @JsonKey(name: 'links') LinksEntity? links,
    @JsonKey(name: 'rocket') RocketDetailsEntity? rocket,
  }) = _LaunchEntity;

  factory LaunchEntity.fromJson(Map<String, dynamic> json) => _$LaunchEntityFromJson(json);
}
