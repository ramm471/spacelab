import 'package:freezed_annotation/freezed_annotation.dart';

part 'links_entity.freezed.dart';

part 'links_entity.g.dart';

@Freezed()
class LinksEntity with _$LinksEntity {
  @JsonSerializable(explicitToJson: true)
  const factory LinksEntity({
    @JsonKey(name: 'article_link') String? articleLink,
    @JsonKey(name: 'wikipedia') String? wikiLink,
    @JsonKey(name: 'flickr_images') List<String>? images,
  }) = _LinksEntity;

  factory LinksEntity.fromJson(Map<String, dynamic> json) => _$LinksEntityFromJson(json);
}
