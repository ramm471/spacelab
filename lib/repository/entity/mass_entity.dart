import 'package:freezed_annotation/freezed_annotation.dart';

part 'mass_entity.freezed.dart';

part 'mass_entity.g.dart';

@Freezed()
class MassEntity with _$MassEntity {
  @JsonSerializable(explicitToJson: true)
  const factory MassEntity({
    @JsonKey(name: 'kg') int? massKg,
  }) = _MassEntity;

  factory MassEntity.fromJson(Map<String, dynamic> json) => _$MassEntityFromJson(json);
}
