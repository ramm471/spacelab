import 'dart:async';
import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:graphql/client.dart';

import 'entity/launch_entity.dart';

abstract class LaunchesParser {
  Future<List<LaunchEntity>> parseLaunchesResponse(QueryResult result);

  Future<LaunchEntity> parseLaunchResponse(QueryResult result);
}

class LaunchesParserImpl implements LaunchesParser {
  @override
  Future<List<LaunchEntity>> parseLaunchesResponse(QueryResult result) async {
    debugPrint("loadLaunches result ${result.data}");
    final List<LaunchEntity> entities = [];
    if (result.hasException) {
      debugPrint("loadLaunches error ${result.exception?.toString()} error: ${result.exception}");
      throw Exception("loadLaunches error ${result.exception?.toString()}");
    } else {
      try {
        debugPrint("loadLaunches parsing ${result.data}");
        final List<LaunchEntity> parsedLaunches = (result.data?['launchesPast'] as List<dynamic>?)
                ?.map((e) => LaunchEntity.fromJson(Map<String, Object>.from(e)))
                .toList() ??
            [];
        entities.addAll(parsedLaunches);
        debugPrint("loadLaunches repositories entities: $entities");
      } catch (e) {
        debugPrint("loadLaunches error parsing $e error: ${result.exception}");
        throw Exception("loadLaunches parsing error ${result.exception?.toString()}");
      }
    }

    return entities;
  }

  @override
  Future<LaunchEntity> parseLaunchResponse(QueryResult result) async {
    if (result.hasException) {
      debugPrint("loadLaunch error ${result.exception?.toString()}");
      throw Exception("loadLaunch error ${result.exception?.toString()}");
    } else {
      debugPrint("loadLaunch parsing ${result.data}");
      return LaunchEntity.fromJson(Map<String, Object?>.from(result.data?['launch']));
    }
  }
}
