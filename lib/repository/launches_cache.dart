import 'dart:collection';
import 'dart:convert';
import 'dart:core';

import 'package:flutter/foundation.dart' show debugPrint, kIsWeb;
import 'package:shared_preferences/shared_preferences.dart';

import 'entity/launch_entity.dart';

abstract class LaunchesCache {
  Future<List<LaunchEntity>> getLaunches();

  Future<void> setLaunches(List<LaunchEntity> entities);
}

class LaunchesCacheImpl implements LaunchesCache {
  @override
  Future<List<LaunchEntity>> getLaunches() async {
    final result = <LaunchEntity>[];
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      final json = prefs.getString(_launchesKey) ?? "";
      final list = jsonDecode(json)[_launchesKey];
      final List<Map<String, dynamic>>? entitiesJson = list != null ? List.from(list) : null;
      final entities = entitiesJson?.map((e) => LaunchEntity.fromJson(e)).toList() ?? [];
      result.addAll(entities);
    } catch (e) {
      debugPrint("LaunchesCache getLaunches error: $e");
    }
    debugPrint("LaunchesCache getLaunches result $result");
    return result;
  }

  @override
  Future<void> setLaunches(List<LaunchEntity> entities) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      final String json = jsonEncode(LinkedHashSet.from(entities).toList());
      final Map<String, String> launchesJson = {"\"$_launchesKey\"": json};
      await prefs.setString(_launchesKey, launchesJson.toString());
      debugPrint("LaunchesCache setLaunches success");
    } catch (e) {
      debugPrint("LaunchesCache setLaunches error: $e");
    }
  }
}

const String _launchesKey = "_LAUNCHES_KEY";
