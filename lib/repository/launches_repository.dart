import 'dart:async';
import 'dart:core';

import 'package:flutter/foundation.dart' show debugPrint, kIsWeb;
import 'package:graphql/client.dart';
import 'package:space_lab/service/launches_service.dart';

import 'entity/launch_entity.dart';
import 'launches_cache.dart';
import 'launches_parser.dart';

abstract class LaunchesRepository {
  Future<List<LaunchEntity>> loadLaunches({
    required int offset,
  });

  Future<LaunchEntity> loadLaunchDetails(String id);
}

class LaunchesRepositoryImpl implements LaunchesRepository {
  final LaunchesService _service;
  final LaunchesParser _parser;
  final LaunchesCache _launchesCache;

  LaunchesRepositoryImpl({
    required LaunchesService service,
    required LaunchesParser parser,
    required LaunchesCache cache,
  })  : _service = service,
        _parser = parser,
        _launchesCache = cache;

  @override
  Future<List<LaunchEntity>> loadLaunches({
    required int offset,
  }) async {
    debugPrint("loadLaunches offset: $offset;");

    final cachedLaunches = await _launchesCache.getLaunches();
    if (cachedLaunches.length >= offset + launchesLimit) {
      final entities =
          (offset != 0) ? cachedLaunches.getRange(offset, offset + launchesLimit).toList() : cachedLaunches;
      debugPrint("loadLaunches return cached entities(${entities.length}): $entities;");
      return entities;
    } else {
      final QueryResult result = await _service.loadLaunches(offset: offset);
      final entities = await _parser.parseLaunchesResponse(result);
      _updateCache(entities);
      debugPrint("loadLaunches return network entities(${entities.length}): $entities;");
      return entities;
    }
  }

  @override
  Future<LaunchEntity> loadLaunchDetails(String id) async {
    final QueryResult result = await _service.loadLaunchDetails(id);
    return _parser.parseLaunchResponse(result);
  }

  void _updateCache(List<LaunchEntity> newItems) async {
    final cache = await _launchesCache.getLaunches();
    cache.addAll(newItems);
    await _launchesCache.setLaunches(cache);
  }
}
