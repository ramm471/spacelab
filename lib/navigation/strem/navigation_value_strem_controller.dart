import 'dart:async';

abstract class NavigationProvider<V> {
  Stream<V> get selectedValue;

  void selectValue(V id);
}

class NavigationProviderImpl<V> extends NavigationProvider<V> {
  final StreamController<V> _streamController = StreamController<V>();

  @override
  Stream<V> get selectedValue => _streamController.stream;

  @override
  void selectValue(V id) {
    _streamController.add(id);
  }
}
