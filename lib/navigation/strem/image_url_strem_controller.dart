import 'package:space_lab/navigation/strem/navigation_value_strem_controller.dart';

abstract class ImageUrlProvider extends NavigationProviderImpl<String> {}

class ImageUrlProviderImpl extends ImageUrlProvider {}
