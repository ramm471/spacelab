import 'navigation_value_strem_controller.dart';

abstract class LaunchIdProvider extends NavigationProviderImpl<String> {}

class LaunchIdProviderImpl extends LaunchIdProvider {}
