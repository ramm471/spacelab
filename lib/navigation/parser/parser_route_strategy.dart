import 'package:flutter/cupertino.dart';

import 'space_lab_route_info_parser.dart';

abstract class ParserRouteStrategy {
  bool isRoutePath(Uri? uri);

  SpaceLabRoutePath? mapToRoutePath(Uri? uri);

  bool isRouteInformation(SpaceLabRoutePath configuration);

  RouteInformation? restoreRouteInformation(SpaceLabRoutePath configuration);
}
