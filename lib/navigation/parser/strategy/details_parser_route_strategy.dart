import 'package:flutter/cupertino.dart';
import 'package:space_lab/navigation/parser/parser_route_strategy.dart';

import '../space_lab_route_info_parser.dart';

class DetailsParserRouteStrategy implements ParserRouteStrategy {
  @override
  bool isRoutePath(Uri? uri) {
    return _isDetailsUri(uri);
  }

  @override
  SpaceLabRoutePath? mapToRoutePath(Uri? uri) {
    if (uri != null && _isDetailsUri(uri)) {
      return SpaceLabRoutePath.details(
        launchId: uri.pathSegments[_detailsPathSegmentNumber],
      );
    }
    return null;
  }

  @override
  RouteInformation? restoreRouteInformation(SpaceLabRoutePath configuration) {
    if (isRouteInformation(configuration)) {
      return RouteInformation(location: '$detailsUrl/${configuration.launchId}');
    }
    return null;
  }

  @override
  bool isRouteInformation(SpaceLabRoutePath configuration) {
    return configuration.launchId != null && configuration.imageUrl == null;
  }

  bool _isDetailsUri(Uri? uri) {
    return uri != null &&
        uri.pathSegments.length >= 2 &&
        uri.pathSegments[_detailsUrlPathSegmentNumber] == detailsUrl.replaceAll("/", "");
  }
}

const String detailsUrl = "/details";
const int _detailsPathSegmentNumber = 1;
const int _detailsUrlPathSegmentNumber = 0;
