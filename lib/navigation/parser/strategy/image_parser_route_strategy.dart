import 'package:flutter/cupertino.dart';
import 'package:space_lab/navigation/parser/parser_route_strategy.dart';

import '../space_lab_route_info_parser.dart';
import 'details_parser_route_strategy.dart';

class ImageParserRouteStrategy implements ParserRouteStrategy {
  @override
  bool isRoutePath(Uri? uri) {
    return _isImageUri(uri);
  }

  @override
  SpaceLabRoutePath? mapToRoutePath(Uri? uri) {
    if (uri != null && _isImageUri(uri)) {
      return SpaceLabRoutePath.image(
        launchId: uri.pathSegments[_detailsPathSegmentNumber],
        imageUrl: uri.pathSegments.getRange(_imagePathSegmentNumber, uri.pathSegments.length).join("/"),
      );
    }
    return null;
  }

  @override
  RouteInformation? restoreRouteInformation(SpaceLabRoutePath configuration) {
    if (isRouteInformation(configuration)) {
      return RouteInformation(location: '$detailsUrl/${configuration.launchId}$imageUrl/${configuration.imageUrl}');
    }
    return null;
  }

  @override
  bool isRouteInformation(SpaceLabRoutePath configuration) {
    return configuration.launchId != null && configuration.imageUrl != null;
  }

  bool _isImageUri(Uri? uri) {
    return uri != null &&
        uri.pathSegments.length >= 4 &&
        uri.pathSegments[_detailsUrlPathSegmentNumber] == detailsUrl.replaceAll("/", "") &&
        uri.pathSegments[_imageUrlPathSegmentNumber] == imageUrl.replaceAll("/", "");
  }
}

const String imageUrl = "/image";

const int _detailsPathSegmentNumber = 1;
const int _detailsUrlPathSegmentNumber = 0;
const int _imagePathSegmentNumber = 3;
const int _imageUrlPathSegmentNumber = 2;
