import 'package:flutter/cupertino.dart';
import 'package:space_lab/navigation/parser/parser_route_strategy.dart';

import '../space_lab_route_info_parser.dart';

class HomeParserRouteStrategy implements ParserRouteStrategy {
  @override
  bool isRoutePath(Uri? uri) {
    return _isHomeUri(uri);
  }

  @override
  SpaceLabRoutePath? mapToRoutePath(Uri? uri) {
    if (uri != null && _isHomeUri(uri)) {
      return SpaceLabRoutePath.home();
    }
    return null;
  }

  @override
  RouteInformation? restoreRouteInformation(SpaceLabRoutePath configuration) {
    if (isRouteInformation(configuration)) {
      return const RouteInformation(location: homeUrl);
    }
    return null;
  }

  bool _isHomeUri(Uri? uri) {
    debugPrint("HomeParserRouteStrategy _isHomeUri $uri");
    return uri != null && uri.pathSegments.isEmpty;
  }

  @override
  bool isRouteInformation(SpaceLabRoutePath configuration) {
    debugPrint("HomeParserRouteStrategy isRouteInformation $configuration");
    return configuration.launchId == null && configuration.imageUrl == null;
  }
}

const String homeUrl = "/list";
