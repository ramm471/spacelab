import 'package:space_lab/navigation/parser/parser_route_strategy.dart';
import 'package:space_lab/navigation/parser/strategy/details_parser_route_strategy.dart';
import 'package:space_lab/navigation/parser/strategy/home_parser_route_strategy.dart';
import 'package:space_lab/navigation/parser/strategy/image_parser_route_strategy.dart';

abstract class ParserRouteStrategyHelper {
  void addStrategy(ParserRouteStrategy strategy);

  List<ParserRouteStrategy> getAllStrategies();

  ParserRouteStrategy getDefaultStrategy();
}

class ParserRouteStrategyHelperImpl implements ParserRouteStrategyHelper {
  final List<ParserRouteStrategy> _strategies = [
    HomeParserRouteStrategy(),
    DetailsParserRouteStrategy(),
    ImageParserRouteStrategy(),
  ];

  @override
  void addStrategy(ParserRouteStrategy strategy) {
    if (!_strategies.contains(strategy)) {
      _strategies.add(strategy);
    }
  }

  @override
  List<ParserRouteStrategy> getAllStrategies() => _strategies;

  @override
  ParserRouteStrategy getDefaultStrategy() {
    return HomeParserRouteStrategy();
  }
}
