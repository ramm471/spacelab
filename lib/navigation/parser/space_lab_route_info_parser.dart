import 'package:flutter/material.dart';
import 'package:space_lab/navigation/parser/parser_route_strategy_helper.dart';
import 'package:space_lab/navigation/parser/strategy/home_parser_route_strategy.dart';
import 'package:space_lab/utils/utils.dart';

class SpaceLabRouteInformationParser extends RouteInformationParser<SpaceLabRoutePath> {
  final ParserRouteStrategyHelper _parserRouteStrategyHelper;

  const SpaceLabRouteInformationParser({
    required ParserRouteStrategyHelper parserRouteStrategyHelper,
  }) : _parserRouteStrategyHelper = parserRouteStrategyHelper;

  @override
  Future<SpaceLabRoutePath> parseRouteInformation(RouteInformation routeInformation) async {
    final uri = Uri.tryParse(routeInformation.location.orEmpty);
    debugPrint("parseRouteInformation $uri");
    return _parserRouteStrategyHelper
            .getAllStrategies()
            .firstWhere(
              (strategy) => strategy.isRoutePath(uri),
              orElse: () => _parserRouteStrategyHelper.getDefaultStrategy(),
            )
            .mapToRoutePath(uri) ??
        SpaceLabRoutePath.home();
  }

  @override
  RouteInformation? restoreRouteInformation(SpaceLabRoutePath configuration) {
    debugPrint("restoreRouteInformation ${configuration.launchId}");
    return _parserRouteStrategyHelper
            .getAllStrategies()
            .firstWhere(
              (strategy) => strategy.isRouteInformation(configuration),
              orElse: () => _parserRouteStrategyHelper.getDefaultStrategy(),
            )
            .restoreRouteInformation(configuration) ??
        const RouteInformation(location: homeUrl);
  }
}

class SpaceLabRoutePath {
  final String? launchId;
  final String? imageUrl;

  SpaceLabRoutePath.home()
      : launchId = null,
        imageUrl = null;

  SpaceLabRoutePath.details({required this.launchId}) : imageUrl = null;

  SpaceLabRoutePath.image({required this.launchId, required this.imageUrl});
}
