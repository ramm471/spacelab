import 'package:flutter/cupertino.dart';
import 'package:space_lab/navigation/space_lab_router_delegate.dart';

class SpaceLabBackButtonDispatcher extends RootBackButtonDispatcher {
  final SpaceLabRouterDelegate _routerDelegate;

  SpaceLabBackButtonDispatcher({
    required SpaceLabRouterDelegate routerDelegate,
  }) : _routerDelegate = routerDelegate;

  @override
  Future<bool> didPopRoute() async {
    if (_routerDelegate.onBackButtonPressed()) {
      return true;
    }
    return await invokeCallback(Future<bool>.value(false));
  }
}
