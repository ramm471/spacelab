import 'package:flutter/material.dart';
import 'package:space_lab/utils/keys.dart';

import '../../home/home_screen.dart';

typedef DetailsId = String? Function();

class HomePage extends Page {
  final DetailsId _detailsId;

  const HomePage({
    required DetailsId detailsId,
  })  : _detailsId = detailsId,
        super(key: const ValueKey("HomePage"));

  @override
  Route createRoute(BuildContext context) {
    debugPrint("HomePage createRoute ${_detailsId()}");
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        final detailsId = _detailsId();
        debugPrint("HomePage builder $detailsId");
        return HomeScreen(
          key: SpaceLabKeys.launchesScreenKey,
          detailsId: detailsId,
        );
      },
    );
  }
}
