import 'package:flutter/material.dart';
import 'package:space_lab/image/image_screen.dart';
import 'package:space_lab/utils/keys.dart';

typedef ImageUrl = String Function();

class ImagePage extends Page {
  final ImageUrl _imageUrl;

  const ImagePage({
    required ImageUrl imageUrl,
  })  : _imageUrl = imageUrl,
        super(key: const ValueKey("ImagePage"));

  @override
  Route createRoute(BuildContext context) {
    debugPrint("ImagePage createRoute ${_imageUrl()}");
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        final imageUrl = _imageUrl();
        debugPrint("ImagePage builder $imageUrl");
        return ImageScreen(
          key: SpaceLabKeys.imageScreen,
          url: imageUrl,
        );
      },
    );
  }
}
