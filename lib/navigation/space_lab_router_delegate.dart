import 'dart:async';

import 'package:flutter/material.dart';
import 'package:space_lab/navigation/page/home_page.dart';
import 'package:space_lab/navigation/page/image_page.dart';
import 'package:space_lab/navigation/parser/space_lab_route_info_parser.dart';
import 'package:space_lab/navigation/strem/image_url_strem_controller.dart';
import 'package:space_lab/navigation/strem/launch_id_strem_controller.dart';

class SpaceLabRouterDelegate extends RouterDelegate<SpaceLabRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<SpaceLabRoutePath> {
  late final StreamSubscription? _launchIdProviderSubscription;
  late final StreamSubscription? _imageUrlProviderSubscription;

  String? _selectedLaunchId;
  String? _selectedImageUrl;

  SpaceLabRouterDelegate({
    required LaunchIdProvider launchIdProvider,
    required ImageUrlProvider imageUrlProvider,
  }) {
    _launchIdProviderSubscription = launchIdProvider.selectedValue.listen((id) {
      _onLaunchItemTap(id);
    });
    _imageUrlProviderSubscription = imageUrlProvider.selectedValue.listen((url) {
      _onImageItemTap(url);
    });
  }

  @override
  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  void dispose() {
    debugPrint("dispose");
    _launchIdProviderSubscription?.cancel();
    _imageUrlProviderSubscription?.cancel();
    super.dispose();
  }

  @override
  SpaceLabRoutePath get currentConfiguration {
    if (_selectedImageUrl != null) {
      return SpaceLabRoutePath.image(launchId: _selectedLaunchId, imageUrl: _selectedImageUrl);
    } else if (_selectedLaunchId != null) {
      return SpaceLabRoutePath.details(launchId: _selectedLaunchId);
    } else {
      return SpaceLabRoutePath.home();
    }
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("RouterDelegate build $_selectedLaunchId");
    return Navigator(
      key: navigatorKey,
      pages: _buildPages(),
      onPopPage: _onPopPage,
    );
  }

  @override
  Future<void> setNewRoutePath(configuration) async {
    debugPrint("RouterDelegate setNewRoutePath $configuration");
    _selectedLaunchId = configuration.launchId;
    _selectedImageUrl = configuration.imageUrl;
  }

  bool onBackButtonPressed() {
    debugPrint("RouterDelegate onBackButtonPressed $_selectedLaunchId");
    if (_selectedLaunchId != null && _selectedImageUrl == null) {
      _selectedLaunchId = null;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  List<Page> _buildPages() {
    debugPrint("RouterDelegate _createPages, id: $_selectedLaunchId;");
    final imageUrl = _selectedImageUrl;
    return [
      HomePage(
        detailsId: () => _selectedLaunchId,
      ),
      if (imageUrl != null) ImagePage(imageUrl: () => imageUrl),
    ];
  }

  void _onLaunchItemTap(String id) {
    debugPrint("_onLaunchItemTap $id");
    _selectedLaunchId = id;
    notifyListeners();
  }

  void _onImageItemTap(String url) {
    debugPrint("_onImageItemTap $url");
    _selectedImageUrl = url.replaceAll("http://", "").replaceAll("https://", "");
    notifyListeners();
  }

  bool _onPopPage(Route route, dynamic result) {
    debugPrint("RouterDelegate _onPopPage $_selectedLaunchId; $_selectedImageUrl");
    if (!route.didPop(result)) return false;
    if (_selectedImageUrl != null) {
      _selectedImageUrl = null;
    } else if (_selectedLaunchId != null) {
      _selectedLaunchId = null;
    }
    debugPrint("RouterDelegate _onPopPage $_selectedLaunchId; $_selectedImageUrl return true");
    notifyListeners();
    return true;
  }
}
