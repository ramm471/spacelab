import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:space_lab/navigation/back_button_dispatcher.dart';

class HomeScreenBackButton extends StatelessWidget {
  const HomeScreenBackButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back_ios),
      onPressed: () {
        context.read<SpaceLabBackButtonDispatcher>().didPopRoute();
      },
    );
  }
}
