import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:space_lab/details/bloc/details_cubit.dart';
import 'package:space_lab/details/details_screen.dart';
import 'package:space_lab/list/bloc/list_cubit.dart';
import 'package:space_lab/list/launches_screen.dart';
import 'package:space_lab/repository/launches_repository.dart';
import 'package:space_lab/utils/keys.dart';
import 'package:space_lab/utils/localization/localization.dart';
import 'package:space_lab/utils/utils.dart';

import 'home_screen_back_button.dart';

class HomeScreen extends StatelessWidget {
  final String? _detailsId;

  const HomeScreen({
    Key? key,
    String? detailsId,
  })  : _detailsId = detailsId,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final isDetails = _detailsId != null;
    debugPrint("HomeScreen build isDetails: $isDetails;");
    return Scaffold(
      key: SpaceLabKeys.homeScaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: isDetails ? const HomeScreenBackButton() : null,
        backgroundColor: Colors.black,
        title: Text((SpaceLabLocalizations.of(context)?.appTitle).orEmpty),
      ),
      body: ResponsiveBuilder(
        builder: (context, sizingInformation) {
          final screenWidth = sizingInformation.screenSize.width;
          final isLarge = sizingInformation.deviceScreenType == DeviceScreenType.desktop ||
              sizingInformation.deviceScreenType == DeviceScreenType.tablet;
          debugPrint("HomeScreen ResponsiveBuilder isDetails: $isDetails; isLarge: $isLarge");
          return Container(
            key: SpaceLabKeys.homeContainerKey,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.deepPurple[50] ?? Colors.grey,
                  Colors.indigo[100] ?? Colors.grey,
                  Colors.blue[100] ?? Colors.grey,
                  Colors.cyanAccent[100] ?? Colors.grey,
                ],
              ),
            ),
            child: Row(
              key: SpaceLabKeys.homeRowKey,
              children: [
                if (!isDetails || isLarge)
                  SizedBox(
                    key: SpaceLabKeys.launchesScreenBoxKey,
                    width: (isLarge && isDetails) ? (screenWidth / 3) : screenWidth,
                    child: BlocProvider(
                      create: (context) => ListCubit(
                        launchesRepository: context.read<LaunchesRepository>(),
                      ),
                      child: const ListScreen(
                        key: SpaceLabKeys.launchesScreenKey,
                      ),
                    ),
                  ),
                if (isDetails)
                  SizedBox(
                    key: SpaceLabKeys.detailsScreenBoxKey(_detailsId ?? ""),
                    width: isLarge ? (screenWidth / 3 * 2) : screenWidth,
                    child: BlocProvider(
                      create: (context) =>
                          DetailsCubit(launchesRepository: context.read<LaunchesRepository>(), id: _detailsId)
                            ..loadDetails(),
                      child: DetailsScreen(
                        key: SpaceLabKeys.detailsScreenKey(_detailsId ?? ""),
                      ),
                    ),
                  ),
              ],
            ),
          );
        },
      ),
    );
  }
}
