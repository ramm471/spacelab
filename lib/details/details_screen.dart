import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:space_lab/details/bloc/details_cubit.dart';
import 'package:space_lab/details/bloc/details_state.dart';
import 'package:space_lab/details/widget/details_widget.dart';
import 'package:space_lab/utils/localization/localization.dart';
import 'package:space_lab/utils/utils.dart';

class DetailsScreen extends StatelessWidget {
  const DetailsScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailsCubit, DetailsState>(
      builder: (context, state) {
        if (state is DetailsScreenInitialState) {
          return Center(
            child: Text((SpaceLabLocalizations.of(context)?.launchDetailsEmpty).orEmpty),
          );
        } else if (state is DetailsScreenLoadingState) {
          return Center(
            child: CircularProgressIndicator(
              color: Colors.deepPurple[600] ?? Colors.deepPurple,
              backgroundColor: Colors.transparent,
            ),
          );
        } else if (state is DetailsScreenErrorState) {
          return Center(
            child: Text((SpaceLabLocalizations.of(context)?.error).orEmpty),
          );
        } else if (state is DetailsLoadedState) {
          return DetailsWidget(
            launchItem: state.launch,
          );
        } else {
          return Center(
            child: Text((SpaceLabLocalizations.of(context)?.error).orEmpty),
          );
        }
      },
    );
  }
}
