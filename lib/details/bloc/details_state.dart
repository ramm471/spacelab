import 'package:equatable/equatable.dart';
import 'package:space_lab/list/model/launch_model.dart';

abstract class DetailsState extends Equatable {
  const DetailsState();

  @override
  List<Object> get props => [];
}

class DetailsScreenInitialState extends DetailsState {
  const DetailsScreenInitialState() : super();
}

class DetailsScreenLoadingState extends DetailsState {
  const DetailsScreenLoadingState() : super();
}

class DetailsScreenErrorState extends DetailsState {
  const DetailsScreenErrorState() : super();
}

class DetailsLoadedState extends DetailsState {
  final SpaceLaunch launch;

  const DetailsLoadedState({
    required this.launch,
  });

  @override
  List<Object> get props => [launch];

  @override
  String toString() {
    return 'DetailsLoadedState{launch: $launch';
  }
}
