import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:space_lab/list/model/launch_model.dart';
import 'package:space_lab/repository/entity/launch_entity.dart';
import 'package:space_lab/repository/launches_repository.dart';

import 'details_state.dart';

class DetailsCubit extends Cubit<DetailsState> {
  final String? _id;
  final LaunchesRepository _launchesRepository;

  DetailsCubit({
    required LaunchesRepository launchesRepository,
    required String? id,
  })  : _launchesRepository = launchesRepository,
        _id = id,
        super(const DetailsScreenInitialState());

  Future<void> loadDetails() async {
    debugPrint("loadDetails");
    final launchId = _id;
    if (launchId == null) {
      if (state is! DetailsScreenInitialState) {
        emit(const DetailsScreenInitialState());
      }
      return;
    }
    if (state is DetailsScreenLoadingState) return;
    final previousState = state;
    if (previousState is DetailsLoadedState && launchId == previousState.launch.id) return;
    emit(const DetailsScreenLoadingState());
    var entity = await _launchesRepository.loadLaunchDetails(launchId).onError((error, stackTrace) => LaunchEntity(
          id: "",
          missionName: "",
          launchDateUtc: DateTime.now(),
        ));
    debugPrint("loadDetails entity: $entity");
    if (entity.id.isNotEmpty) {
      emit(DetailsLoadedState(launch: SpaceLaunch.fromEntity(entity)));
    } else {
      emit(const DetailsScreenErrorState());
    }
  }
}
