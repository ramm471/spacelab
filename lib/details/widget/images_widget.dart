import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:space_lab/navigation/strem/image_url_strem_controller.dart';
import 'package:space_lab/utils/utils.dart';

class ImagesWidget extends StatelessWidget {
  final List<String>? _images;

  const ImagesWidget({
    Key? key,
    required List<String>? images,
  })  : _images = images,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("build $_images");
    return SizedBox(
      height: imageSize(context),
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: _images?.length ?? 0,
        scrollDirection: Axis.horizontal,
        addAutomaticKeepAlives: true,
        itemBuilder: (BuildContext context, int index) {
          final url = (_images?[index]).orEmpty;
          return Padding(
            padding: const EdgeInsets.all(8),
            child: GestureDetector(
              child: HeroMode(
                child: Hero(
                  tag: url,
                  child: CachedNetworkImage(
                    imageUrl: url,
                    width: imageSize(context),
                    height: imageSize(context),
                    fit: BoxFit.fill,
                    errorWidget: (context, url, error) => const Icon(Icons.error),
                  ),
                ),
              ),
              onTap: () {
                context.read<ImageUrlProvider>().selectValue(url);
              },
            ),
          );
        },
      ),
    );
  }
}

double imageSize(BuildContext context) =>
    kIsWeb ? (MediaQuery.of(context).size.width / 5) : (MediaQuery.of(context).size.width / 3);
