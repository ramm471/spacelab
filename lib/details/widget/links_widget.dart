import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:space_lab/utils/localization/localization.dart';
import 'package:space_lab/utils/style/text_style.dart';
import 'package:space_lab/utils/utils.dart';
import 'package:url_launcher/url_launcher.dart';

class LinksWidget extends StatelessWidget {
  final List<String> _links;

  const LinksWidget({
    Key? key,
    required List<String> links,
  })  : _links = links,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            top: 16,
          ),
          child: Text(
            (SpaceLabLocalizations.of(context)?.links).orEmpty,
            style: SpaceLabTextStyle.headline3AccentTextStyle(context),
          ),
        ),
        ..._links.map((e) => _getLinkWidget(context: context, link: e)),
      ],
    );
  }
}

Widget _getLinkWidget({
  required BuildContext context,
  required String link,
}) {
  return InkWell(
    child: Padding(
      padding: const EdgeInsets.only(
        top: 8,
        bottom: 8,
      ),
      child: Text(
        link,
        maxLines: 1,
        softWrap: true,
        style: SpaceLabTextStyle.headline4LinkTextStyle(context),
      ),
    ),
    onTap: () async {
      await launch(link);
    },
  );
}
