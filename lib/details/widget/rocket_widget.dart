import 'package:flutter/material.dart';
import 'package:space_lab/details/widget/rocket_description_widget.dart';
import 'package:space_lab/list/model/rocket_model.dart';
import 'package:space_lab/utils/localization/localization.dart';
import 'package:space_lab/utils/style/text_style.dart';
import 'package:space_lab/utils/utils.dart';

class RocketWidget extends StatelessWidget {
  final Rocket? _rocket;

  const RocketWidget({
    Key? key,
    Rocket? rocket,
  })  : _rocket = rocket,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("build $_rocket");
    var localization = SpaceLabLocalizations.of(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            top: 16,
          ),
          child: Text(
            (SpaceLabLocalizations.of(context)?.rocket).orEmpty,
            style: SpaceLabTextStyle.headline3AccentTextStyle(context),
          ),
        ),
        RocketDescriptionWidget(
          description: "${localization?.rocketName}",
          value: "${_rocket?.name.orEmpty}",
        ),
        RocketDescriptionWidget(
          description: "${localization?.rocketCountry}",
          value: "${_rocket?.country.orEmpty}",
        ),
        RocketDescriptionWidget(
          description: "${localization?.rocketCompany}",
          value: "${_rocket?.company.orEmpty}",
        ),
        RocketDescriptionWidget(
          description: "${localization?.rocketMass}",
          value: "${_rocket?.mass.toString().orEmpty}",
        ),
        Padding(
          padding: const EdgeInsets.only(
            top: 8,
            bottom: 8,
          ),
          child: Text(
            (_rocket?.description).orEmpty,
            style: SpaceLabTextStyle.subtitle1AccentThirdlyTextStyle(context),
          ),
        ),
      ],
    );
  }
}
