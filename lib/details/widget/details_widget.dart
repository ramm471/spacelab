import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:space_lab/details/widget/rocket_widget.dart';
import 'package:space_lab/list/model/launch_model.dart';
import 'package:space_lab/utils/keys.dart';
import 'package:space_lab/utils/style/text_style.dart';
import 'package:space_lab/utils/utils.dart';

import 'images_widget.dart';
import 'links_widget.dart';

class DetailsWidget extends StatelessWidget {
  final SpaceLaunch _launchItem;

  const DetailsWidget({
    Key? key,
    required SpaceLaunch launchItem,
  })  : _launchItem = launchItem,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("build $_launchItem");
    return Padding(
      padding: const EdgeInsets.all(16),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: viewportConstraints.maxHeight,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AnimatedTextKit(
                    animatedTexts: [
                      TypewriterAnimatedText(
                        (_launchItem.missionName).orEmpty,
                        textStyle: SpaceLabTextStyle.headline1AccentTextStyle(context),
                        speed: const Duration(milliseconds: 50),
                        cursor: "",
                      ),
                    ],
                    isRepeatingAnimation: false,
                  ),
                  AnimatedTextKit(
                    animatedTexts: [
                      TypewriterAnimatedText(
                        (_launchItem.formattedDate).orEmpty,
                        textStyle: SpaceLabTextStyle.headline2AccentTextStyle(context),
                        speed: const Duration(milliseconds: 50),
                        cursor: "",
                      ),
                    ],
                    isRepeatingAnimation: false,
                  ),
                  if (_launchItem.images?.isNotEmpty == true)
                    ImagesWidget(
                      key: SpaceLabKeys.detailsImages,
                      images: _launchItem.images,
                    ),
                  RocketWidget(
                    key: SpaceLabKeys.detailsRocket,
                    rocket: _launchItem.rocket,
                  ),
                  if (_launchItem.links?.isNotEmpty == true)
                    LinksWidget(
                      key: SpaceLabKeys.detailsLinks,
                      links: _launchItem.links ?? [],
                    ),
                  if (_launchItem.missionDescription?.isNotEmpty == true)
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: AnimatedTextKit(
                        animatedTexts: [
                          TyperAnimatedText(
                            (_launchItem.missionDescription).orEmpty,
                            textStyle: SpaceLabTextStyle.headline4AccentThirdlyTextStyle(context),
                            speed: const Duration(milliseconds: 20),
                          ),
                        ],
                        isRepeatingAnimation: false,
                      ),
                    ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
