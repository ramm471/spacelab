import 'package:flutter/material.dart';
import 'package:space_lab/utils/style/text_style.dart';

class RocketDescriptionWidget extends StatelessWidget {
  final String _description;
  final String _value;

  const RocketDescriptionWidget({
    Key? key,
    required String description,
    required String value,
  })  : _description = description,
        _value = value,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("build $_description - $_value");
    return Padding(
      padding: const EdgeInsets.only(
        top: 8,
        bottom: 8,
      ),
      child: Text.rich(
        TextSpan(
          text: '$_description ',
          children: <InlineSpan>[
            TextSpan(
              text: _value,
              style: SpaceLabTextStyle.subtitle2AccentThirdlyTextStyle(context),
            )
          ],
        ),
        style: SpaceLabTextStyle.subtitle1AccentSecondaryTextStyle(context),
      ),
    );
  }
}
