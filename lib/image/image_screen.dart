import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:space_lab/utils/keys.dart';
import 'package:space_lab/utils/localization/localization.dart';
import 'package:space_lab/utils/utils.dart';

class ImageScreen extends StatelessWidget {
  final String _url;

  ImageScreen({Key? key, required String url})
      : _url = (url.startsWith("http") == true ? url : "https://$url"),
        super(key: key ?? SpaceLabKeys.imageScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text((SpaceLabLocalizations.of(context)?.appTitle).orEmpty),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Center(
        child: HeroMode(
          child: Hero(
            tag: _url,
            child: PhotoView(
              imageProvider: CachedNetworkImageProvider(_url),
            ),
          ),
        ),
      ),
    );
  }
}
