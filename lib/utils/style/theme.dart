import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SpaceLabTheme {
  static get theme {
    final originalTextTheme = ThemeData.dark().textTheme;

    return ThemeData.dark().copyWith(
        primaryColor: Colors.grey[300],
        primaryColorDark: Colors.grey[500],
        backgroundColor: Colors.grey[800],
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: originalTextTheme.copyWith(
          headline1: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.headline1?.copyWith(
              fontSize: 36,
              fontWeight: FontWeight.bold,
            ),
          ),
          headline2: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.headline2?.copyWith(
              fontSize: 26,
            ),
          ),
          headline3: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.headline3?.copyWith(
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
          headline4: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.headline4?.copyWith(
              fontSize: 18,
            ),
          ),
          subtitle1: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.subtitle1?.copyWith(
              fontSize: 16,
            ),
          ),
          subtitle2: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.subtitle2?.copyWith(
              fontSize: 14,
            ),
          ),
          bodyText1: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.bodyText1?.copyWith(
              fontSize: 12,
            ),
          ),
          bodyText2: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.bodyText2?.copyWith(
              fontSize: 10,
            ),
          ),
        ),
        colorScheme: ColorScheme.fromSwatch().copyWith(
          secondary: Colors.green[500],
        ));
  }
}
