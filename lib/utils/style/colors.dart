import 'package:flutter/material.dart';

class SpaceLabColors {
  static get textPrimaryColor => Colors.white;

  static get textPrimaryAlternativeColor => Colors.black;

  static get textSecondaryColor => Colors.grey;

  static get textAccentColor => Colors.deepPurple[600] ?? Colors.deepPurple;

  static get textAccentSecondaryColor => Colors.deepPurple[500] ?? Colors.deepPurple;

  static get textAccentThirdlyColor => Colors.deepPurple[400] ?? Colors.deepPurple;

  static get textLinkColor => Colors.blue;
}
