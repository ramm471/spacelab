import 'package:flutter/material.dart';
import 'package:space_lab/utils/style/colors.dart';

class SpaceLabTextStyle {
  static TextStyle? headline1AccentTextStyle(BuildContext context) => Theme.of(context).textTheme.headline1?.copyWith(
        color: SpaceLabColors.textAccentColor,
      );

  static TextStyle? headline2AccentTextStyle(BuildContext context) => Theme.of(context).textTheme.headline2?.copyWith(
        color: SpaceLabColors.textAccentColor,
      );

  static TextStyle? headline3AccentTextStyle(BuildContext context) => Theme.of(context).textTheme.headline3?.copyWith(
        color: SpaceLabColors.textAccentColor,
      );

  static TextStyle? headline4PrimaryTextStyle(BuildContext context) => Theme.of(context).textTheme.headline4?.copyWith(
        color: SpaceLabColors.textPrimaryColor,
      );

  static TextStyle? headline4AccentThirdlyTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.headline4?.copyWith(
            fontStyle: FontStyle.italic,
            color: SpaceLabColors.textAccentThirdlyColor,
          );

  static TextStyle? headline4LinkTextStyle(BuildContext context) => Theme.of(context).textTheme.headline4?.copyWith(
        color: SpaceLabColors.textLinkColor,
      );

  static TextStyle? subtitle2PrimaryTextStyle(BuildContext context) => Theme.of(context).textTheme.subtitle2?.copyWith(
        color: SpaceLabColors.textPrimaryColor,
      );

  static TextStyle? subtitle2PrimaryAlternativeTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.subtitle2?.copyWith(
            color: SpaceLabColors.textPrimaryAlternativeColor,
          );

  static TextStyle? subtitle2SecondaryTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.subtitle2?.copyWith(
            color: SpaceLabColors.textSecondaryColor,
          );

  static TextStyle? subtitle2AccentThirdlyTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.subtitle2?.copyWith(
            fontWeight: FontWeight.w400,
            color: SpaceLabColors.textAccentThirdlyColor,
          );

  static TextStyle? subtitle1AccentSecondaryTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.subtitle1?.copyWith(
            fontWeight: FontWeight.w600,
            color: SpaceLabColors.textAccentSecondaryColor,
          );

  static TextStyle? subtitle1AccentThirdlyTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.subtitle1?.copyWith(
            fontStyle: FontStyle.italic,
            color: SpaceLabColors.textAccentThirdlyColor,
          );

  static TextStyle? body2SecondaryTextStyle(BuildContext context) => Theme.of(context).textTheme.bodyText2?.copyWith(
        color: SpaceLabColors.textSecondaryColor,
      );

  static TextStyle? body1PrimaryTextStyle(BuildContext context) => Theme.of(context).textTheme.bodyText1?.copyWith(
        color: SpaceLabColors.textPrimaryColor,
      );

  static TextStyle? body1PrimaryAlternativeTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.bodyText1?.copyWith(
            color: SpaceLabColors.textPrimaryAlternativeColor,
          );
}
