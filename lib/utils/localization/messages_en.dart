import 'package:intl/message_lookup_by_library.dart';

final messages = MessageLookup();

typedef MessageIfAbsent = Function(String messageStr, List args);

class MessageLookup extends MessageLookupByLibrary {
  @override
  get localeName => 'en';

  @override
  final messages = _notInlinedMessages(_notInlinedMessages);

  static _notInlinedMessages(_) => {
        "appTitle": MessageLookupByLibrary.simpleMessage("Space Lab"),
        "launchDetailsTitle": MessageLookupByLibrary.simpleMessage("Launch Details"),
        "launchDetailsEmpty": MessageLookupByLibrary.simpleMessage("Please select a launch"),
        "links": MessageLookupByLibrary.simpleMessage("Links: "),
        "rocket": MessageLookupByLibrary.simpleMessage("Rocket: "),
        "rocketName": MessageLookupByLibrary.simpleMessage("Name: "),
        "rocketCountry": MessageLookupByLibrary.simpleMessage("Country: "),
        "rocketCompany": MessageLookupByLibrary.simpleMessage("Company: "),
        "rocketMass": MessageLookupByLibrary.simpleMessage("Mass(kg): "),
        "error": MessageLookupByLibrary.simpleMessage("Something went wrong, please try again later."),
        "rocketDiameter": MessageLookupByLibrary.simpleMessage("Diameter"),
        "rocketActive": MessageLookupByLibrary.simpleMessage("Active"),
        "costPL": MessageLookupByLibrary.simpleMessage("Cost PL"),
      };
}
