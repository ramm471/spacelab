
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import 'messages_all.dart';

class SpaceLabLocalizations {
  final Locale _locale;

  SpaceLabLocalizations({required Locale locale}) : _locale = locale;

  static Future<SpaceLabLocalizations> load(Locale locale) {
    return initializeMessages(locale.toString()).then((_) {
      return SpaceLabLocalizations(locale: locale);
    });
  }

  static SpaceLabLocalizations? of(BuildContext context) {
    return Localizations.of<SpaceLabLocalizations>(context, SpaceLabLocalizations);
  }

  String get appTitle => Intl.message(
    'Space Lab',
        name: 'appTitle',
        args: [],
        locale: _locale.toString(),
      );

  String get launchDetailsTitle => Intl.message(
    'Launch Details',
        name: 'launchDetailsTitle',
        args: [],
        locale: _locale.toString(),
      );

  String get launchDetailsEmpty => Intl.message(
    'Please select a launch',
        name: 'launchDetailsEmpty',
        args: [],
        locale: _locale.toString(),
      );

  String get links => Intl.message(
    'Links: ',
        name: 'links',
        args: [],
        locale: _locale.toString(),
      );

  String get rocket => Intl.message(
    'Rocket: ',
        name: 'rocket',
        args: [],
        locale: _locale.toString(),
      );

  String get rocketName => Intl.message(
    'Name: ',
        name: 'rocketName',
        args: [],
        locale: _locale.toString(),
      );

  String get rocketCountry => Intl.message(
    'Country: ',
        name: 'rocketCountry',
        args: [],
        locale: _locale.toString(),
      );

  String get rocketCompany => Intl.message(
    'Company: ',
        name: 'rocketCompany',
        args: [],
        locale: _locale.toString(),
      );

  String get rocketMass => Intl.message(
    'Mass(kg): ',
        name: 'rocketMass',
        args: [],
        locale: _locale.toString(),
      );

  String get error => Intl.message(
    'Something went wrong, please try again later.',
        name: 'error',
        args: [],
        locale: _locale.toString(),
      );

  String get rocketDiameter => Intl.message(
    'Diameter',
        name: 'rocketDiameter',
        args: [],
        locale: _locale.toString(),
      );

  String get rocketActive => Intl.message(
    'Active',
        name: 'rocketActive',
        args: [],
        locale: _locale.toString(),
      );

  String get costPL => Intl.message(
    'Cost P/L',
        name: 'costPL',
        args: [],
        locale: _locale.toString(),
      );
}

class SpaceLabLocalizationsDelegate extends LocalizationsDelegate<SpaceLabLocalizations> {
  Locale? _locale;

  @override
  Future<SpaceLabLocalizations> load(Locale locale) {
    debugPrint("SpaceLabLocalizationsDelegate load $locale");
    _locale = locale;
    return SpaceLabLocalizations.load(locale);
  }

  @override
  bool shouldReload(SpaceLabLocalizationsDelegate old) {
    debugPrint("SpaceLabLocalizationsDelegate shouldReload $old");
    return old._locale != _locale;
  }

  @override
  bool isSupported(Locale locale) {
    debugPrint("SpaceLabLocalizationsDelegate isSupported $locale");
    return locale.languageCode.toLowerCase().contains("en") || locale.languageCode.toLowerCase().contains("ru");
  }
}
