import 'package:intl/message_lookup_by_library.dart';

final messages = MessageLookup();

typedef MessageIfAbsent = Function(String messageStr, List args);

class MessageLookup extends MessageLookupByLibrary {
  @override
  get localeName => 'ru';

  @override
  final messages = _notInlinedMessages(_notInlinedMessages);

  static _notInlinedMessages(_) => {
        "appTitle": MessageLookupByLibrary.simpleMessage("Запуски кораблей"),
        "launchDetailsTitle": MessageLookupByLibrary.simpleMessage("Детали запуска"),
        "launchDetailsEmpty": MessageLookupByLibrary.simpleMessage("Выберите запуск"),
        "links": MessageLookupByLibrary.simpleMessage("Ссылки: "),
        "rocket": MessageLookupByLibrary.simpleMessage("Ракета: "),
        "rocketName": MessageLookupByLibrary.simpleMessage("Название: "),
        "rocketCountry": MessageLookupByLibrary.simpleMessage("Страна: "),
        "rocketCompany": MessageLookupByLibrary.simpleMessage("Компания: "),
        "rocketMass": MessageLookupByLibrary.simpleMessage("Масса(кг): "),
        "error": MessageLookupByLibrary.simpleMessage("Что-то пошло не так, повторите пожалуйста позже."),
        "rocketDiameter": MessageLookupByLibrary.simpleMessage("Диаметр"),
        "rocketActive": MessageLookupByLibrary.simpleMessage("Активно"),
        "costPL": MessageLookupByLibrary.simpleMessage("Цена/з."),
      };
}
