import 'package:flutter/widgets.dart';

class SpaceLabKeys {
  static const appKey = Key('__space_lab_app__');
  static const homeContainerKey = Key('__home_container_key__');
  static const homeScaffoldKey = Key('__home_scaffold_key__');
  static const homeRowKey = Key('__home_row_key__');

  //details
  static detailsScreenKey(String id) => Key('__details_screen_key__${id}__');

  static detailsScreenBoxKey(String id) => Key('__details_screen_box_key__${id}__');
  static const detailsScreenEmptyContainerKey = Key('__details_screen_empty_container_key__');

  //list
  static const launchesScreenKey = Key('__launches_screen_key__');
  static const launchesScreenBoxKey = Key('__launches_screen_box_key__');
  static const launchesScreenEmptyContainerKey = Key('__launches_screen_empty_container_key__');
  static const launchesScreenVisibilityKey = Key('__launches_screen_visibility_key__');
  static const launchesScreenEmptyLoadingKey = Key('__launches_screen_empty_loading_key__');
  static const launchesScreenBottomLoadingKey = Key('__launches_screen_bottom_loading_key__');
  static const launchesScreenListKey = Key('__launches_screen_list_key__');
  static const launchesListKey = Key('__launches_list_key__');

  static launchItem(String id) => Key('launch_item__${id}__');

  static launchItemCard(String id) => Key('launch_item_card__${id}__');

  static launchItemInkWell(String id) => Key('launch_item_ink_well__${id}__');

  //details
  static const detailsLinks = Key('__details_links_key__');
  static const detailsRocket = Key('__details_rocket_key__');
  static const detailsImages = Key('__details_images_key__');

  //image
  static const imageScreen = Key('__image_screen_key__');
}
